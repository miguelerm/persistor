# Persistor

Library to "auto implement" repository interfaces. Inspired by [Refit](https://github.com/paulcbetts/refit) project (which do a similar job for REST services).

## Project Status

It is just an idea, It is not implemented yet.

## How it suppose to work?

Given an interface like:

```cs
interface IEmployeeRepository {
    Task<IEnumerable<EmployeeSummary>> GetAll();
    Task<IPage<EmployeeSummary>> GetAll(PageSettings settings);
    Task<Employee> GetSingle(int id);
    Task<IEnumerable<EmployeeSummary>> FindBy(string name);
    Task<IEnumerable<EmployeeSummary>> FindByTitle(string title);
    Task<IPage<EmployeeSummary>> FindByTitle(string title, PageSettings settings);
    Task<int> Add(Employee employee);
    Task<int> Update(int id, Employee employee);
}
```

It should auto-generate "dynamically" a class that implement the specified interface (based on method's name conventions) to access to a database.

```cs
var repo = Repository.For<IEmployeeRepository>(connection);
var emp = await repo.GetSingle(1);
Console.WriteLine($"Employee: {emp.Name}");
```

## References

* https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html
* https://github.com/paulcbetts/refit
* http://geekswithblogs.net/abhijeetp/archive/2010/04/04/a-simple-dynamic-proxy.aspx
* https://msdn.microsoft.com/en-us/library/system.reflection.emit.methodbuilder(v=vs.110).aspx
* https://sharplab.io/#v2:EYLghgNgTgXgtAMwKZgC4FcpIM5wFYC2ADnEgB5FQA0qIAlhFQCYgDUAPgAIAMABJwEYA3AFgAUD34CALKIl9BAOgAiaMIoDCAewIEtAOzmTBAVjnjOAZn4AmXgBUcqAEpIiW7HVRaoATwCSxBAESPqoaHQGvCC8/o7YLm4eXj6+vADe4rzZvFk5lHQAbmhIvFhgTAYQaf4AiuhIfgCiZEgAxujeULwAjg3NrR1dcjm5YqNWDk6u7p5dAUEhYREGABR1/b4t7Z0+vZvbQz4AlBl5o9moABZ02Ip9jVuDu90AvPuPhy8jOQC+52MJtZOAA2AA8gksYPiiTaPiYAD4EbwAIIQCCrU6ZcYXbLFbpw3RgfRMXjvB4DHZdTTlVBIbREkmYn64wkEYlMTQ6dkkxxkVBk3gAIkgECFLIunAA7B9KUcoIp6o8UdhfPo2qs2RyqLwALJgIhERrHCX/HHZAGTUHQ6bteHIgDKdH0AHMIEhVs6BXQmFiAaN8bwtSTBRSnlSfDSUHSGTymMz/Tlg5zYxy+QL3kLPK73eLExbzRdA0QfYLk1GSgAFMBQMAhOlQBOF0YlznKYD2XxGwVKVThFQdrtIRT+MKWGwSls+xTV2v1xoAOTrpUzPrzzfy04AapAGoKfZOcvmg9yOTOa8uG3cUUx462Tcfj9LZeH5YonTmkCq1Rrkzr9YaxqmgCAIFMUdJSCCUwJK4cJQKSAFGo27Z9mAsHwmUdrwX6G54jWvBaMAeCCvoSAAO7QbC8LHtiuK4v4pLvFgcGcgA4kgqCjqg46rMx8KKOxqAAPLwc6kCrEKDFCscxxUMeoxLiEgp8fBAkcQ6qBQM6Lq8VhbEcSJTBiRiQqKUg0myfJOR9iumEsWpqA2fYdAhLp9mCYZxkSTZFlybhFz+NgTT6GAwDuoxdn8YJABCWhaO6xJuVFBmiSFJmBcFoXhRZx6/Ie2TPoReDAWIZriBY1heo0CBgG0pRxLaswpH44i0Tk1qQjaMF6UiqLok2lrgjC6HwY62nup6YS8D6D6leVEjAnYw16a1lqVVNDEZLwLocUIvDYLtvBmkCUh8GZW07age0HVdR1rbwTkuaUNkXYdN17cd7XWMAcUQLEQUhWFSCkuk21vYdZVzWIFXTWE1W1fVSpyi8q2Fu2qYhhotL0qeTKzYNEICFC9i9Ujvjfuq0IIqs6O46Sf78CYYIoWoI1MDqJO8OygFQKcZFXI0pT2NEtg6qRZFNhMQ1jZ+FNtFTNPABj9N0zqnBMyz4RsxzyLc0hfMC1gDgi5wNhi+RTa/EAA


## License

Licensed under the The [MIT License (MIT)](LICENSE).
