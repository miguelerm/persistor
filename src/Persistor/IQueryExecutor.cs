﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Persistor
{
    public interface IQueryExecutor
    {
        DbCommand CreateCommand();

        Task<IEnumerable<T>> QueryAsync<T>(DbCommand command, Func<DbDataRecord, T> mapper) where T : class, new();
        Task<T> SingleAsync<T>(DbCommand command, Func<DbDataRecord, T> mapper) where T : class, new();
    }
}
