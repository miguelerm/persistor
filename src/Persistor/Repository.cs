﻿using System;
using System.Collections;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Persistor
{
    public static class Repository
    {
        public static T For<T>(IQueryExecutor queryExecutor)
        {
            var assemblyName = new AssemblyName("Persistor.GeneratedAssembly");
            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            Type constructedType = GenerateType<T>(assemblyBuilder);
            var constructorParameters = new object[] { queryExecutor };
            var instance = Activator.CreateInstance(constructedType, constructorParameters);
            return (T)instance;
        }

        private static Type GenerateType<T>(AssemblyBuilder assemblyBuilder)
        {
            var type = typeof(T);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule("Implementations");

            
            var typeBuilder = moduleBuilder.DefineType(type.FullName + "Implementation", TypeAttributes.Public);

            typeBuilder.AddInterfaceImplementation(type);

            var executorField = typeBuilder.DefineField("queryExecutor", typeof(IQueryExecutor), FieldAttributes.InitOnly);

            var ctorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new[] { typeof(IQueryExecutor) });

            var ctorGenerator = ctorBuilder.GetILGenerator();
            ctorGenerator.Emit(OpCodes.Ldarg_0); // this
            ctorGenerator.Emit(OpCodes.Ldarg_1); // first constructor parameter
            ctorGenerator.Emit(OpCodes.Stfld, executorField);
            ctorGenerator.Emit(OpCodes.Ret);

            foreach (var methodInfo in type.GetMethods())
            {
                var methodName = methodInfo.Name;
                var methodParameters = methodInfo.GetParameters();
                var methodParameterTypes = methodInfo.GetParameters().Select(x => x.ParameterType).ToArray();
                var methodAttributes = MethodAttributes.Public | MethodAttributes.Virtual;
                var returnType = methodInfo.ReturnType;

                var methodBuilder = typeBuilder.DefineMethod(methodName, methodAttributes, returnType, methodParameterTypes);
                var methodGenerator = methodBuilder.GetILGenerator();

                var isSingleQuery = true;
                var recordType = returnType.GenericTypeArguments[0];

                if (typeof(IEnumerable).IsAssignableFrom(recordType)) {
                    recordType = recordType.GenericTypeArguments[0];
                    isSingleQuery = false;
                }

                var dbcommand = methodGenerator.DeclareLocal(typeof(DbCommand));
                methodGenerator.DeclareLocal(returnType);

                methodGenerator.Emit(OpCodes.Ldarg_0);
                methodGenerator.Emit(OpCodes.Ldfld, executorField);
                methodGenerator.Emit(OpCodes.Callvirt, executorField.FieldType.GetMethod(nameof(IQueryExecutor.CreateCommand)));

                var query = GenerateSelectQuery(recordType.Name, recordType.GetProperties().Select(x => x.Name).ToArray(), methodParameters.Select(x => x.Name).ToArray());
                methodGenerator.Emit(OpCodes.Stloc_0);
                methodGenerator.Emit(OpCodes.Ldloc_0);
                methodGenerator.Emit(OpCodes.Ldstr, query);
                methodGenerator.Emit(OpCodes.Callvirt, dbcommand.LocalType.GetMethod("set_CommandText"));

                methodGenerator.Emit(OpCodes.Ldarg_0);
                methodGenerator.Emit(OpCodes.Ldfld, executorField);
                methodGenerator.Emit(OpCodes.Ldloc_0);
                methodGenerator.Emit(OpCodes.Ldnull);

                var methodToExecute = isSingleQuery ? nameof(IQueryExecutor.SingleAsync) : nameof(IQueryExecutor.QueryAsync);

                methodGenerator.Emit(OpCodes.Callvirt, executorField.FieldType.GetMethod(methodToExecute).MakeGenericMethod(recordType));
                
                methodGenerator.Emit(OpCodes.Stloc_1);
                methodGenerator.Emit(OpCodes.Ldloc_1);

                methodGenerator.Emit(OpCodes.Ret);
            }

            Type constructedType = typeBuilder.CreateTypeInfo();
            return constructedType;
        }

        private static string GenerateSelectQuery(string table, string[] fields, string[] parameterNames)
        {
            var commaSeparatedFields = string.Join(", ", fields.Select(x => $"[{x}]"));

            var where = new StringBuilder();

            if (parameterNames != null && parameterNames.Length > 0)
            {
                where.Append(" WHERE");

                for (int i = 0; i < parameterNames.Length; i++)
                {
                    var param = parameterNames[0];

                    if (i > 0)
                    {
                        where.Append(" AND");
                    }

                    where.Append($" [{param}] = @p{param}");
                }
            }

            return $"SELECT {commaSeparatedFields} FROM [{table}]{where}";
        }
    }
}
