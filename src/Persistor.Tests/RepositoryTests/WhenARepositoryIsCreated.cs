﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Xunit;

namespace Persistor.Tests.RepositoryTests
{
    [Trait("Repository", "When a repository is created")]
    public class WhenARepositoryIsCreated
    {
        private readonly DbCommand command;
        private readonly ITestRepository repo;

        public WhenARepositoryIsCreated()
        {
            command = CreateDbCommandMock();
            repo = Repository.For<ITestRepository>(new TestQueryExecutor(command));
        }

        [Fact(DisplayName = "Creates an instance of the implemented interface")]
        public void CreatesAnInstanceOfTheImplementedInterface()
        {
            Assert.NotNull(repo);
        }

        [Fact(DisplayName = "Sets the query command")]
        public async Task SetsTheQueryCommand()
        {
            await repo.All();
            Assert.Equal("SELECT [Id], [Name], [Date], [IsEnabled] FROM [TestRecord]", command.CommandText);
        }

        [Fact(DisplayName = "Sets the single command")]
        public async Task SetsTheSingleCommand()
        {
            await repo.Single(0);
            Assert.Equal("SELECT [Id], [Name], [Date], [IsEnabled] FROM [TestRecord] WHERE [id] = @pid", command.CommandText);
        }

        private static DbCommand CreateDbCommandMock()
        {
            return new Moq.Mock<DbCommand>().SetupAllProperties().Object;
        }
    }

    public class TestQueryExecutor : IQueryExecutor
    {
        private readonly DbCommand command;

        public TestQueryExecutor(DbCommand command)
        {
            this.command = command;
        }

        public DbCommand CreateCommand() => command;

        public Task<IEnumerable<T>> QueryAsync<T>(DbCommand command, Func<DbDataRecord, T> mapper) where T : class, new()
        {
            return Task.FromResult(default(IEnumerable<T>));
        }

        public Task<T> SingleAsync<T>(DbCommand command, Func<DbDataRecord, T> mapper) where T : class, new()
        {
            return Task.FromResult(default(T));
        }
    }

    public interface ITestRepository
    {
        Task<IEnumerable<TestRecord>> All();

        Task<TestRecord> Single(int id);
    }

    public class TestRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public bool IsEnabled { get; set; }

    }
}
